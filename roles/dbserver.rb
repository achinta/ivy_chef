name "dbserver"
description "SQL server DB"
run_list "recipe[ivy_sqlserver::default]"
default_attributes "sql_server" => {"accept_eula" => true}
