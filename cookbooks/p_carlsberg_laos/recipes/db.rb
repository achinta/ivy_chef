include_recipe "ivy_sqlserver::default"

#TODO remove hardcoded password
sql_server_connection_info = {
  :host     => '127.0.0.1',
  :port     => node['sql_server']['port'],
  :username => 'sa',
  #:password => node['sql_server']['server_sa_password']
  :password => 'Ivy@123'
}

#create database
sql_server_database 'carlsberg_laos' do
  connection sql_server_connection_info
  action     :create
end

#batch 'dacpac-product' do
  #cwd 'C:\Program Files (x86)\Microsoft SQL Server\110\DAC\bin'
  #code <<-EOH
  #SqlPackage /Action:Publish /SourceFile:"C:\\ivy\\laos\\IvyCpg_Carlsberg_Laos_DBScripts_25_Aug_2015_R61\\IvyCPG_DBProject.dacpac" /TargetDatabaseName:carlsberg_laos /TargetServerName:localhost /TargetUser:sa /TargetPassword:Ivy@123 /Properties:DropIndexesNotInSource=False /Properties:VerifyDeployment=False > "C:\\Windows\\temp\\product_dacpac.txt" 
    #EOH
#end

#TODO get dacpac from build folders. Remove password hardcoding
execute 'dacpac-product' do
  cwd 'C:\Program Files (x86)\Microsoft SQL Server\110\DAC\bin'
  command <<-EOH
  SqlPackage /Action:Publish /SourceFile:"C:\\ivy\\laos\\IvyCpg_Carlsberg_Laos_DBScripts_25_Aug_2015_R61\\IvyCPG_DBProject.dacpac" /TargetDatabaseName:carlsberg_laos /TargetServerName:localhost /TargetUser:sa /TargetPassword:Ivy@123 /Properties:DropIndexesNotInSource=False /Properties:VerifyDeployment=False > "C:\\Windows\\temp\\product_dacpac.txt" 
    EOH
end

#TODO get dacpac from build folders. Remove password hardcoding
#TODO try to use the dacpac cookbook
execute 'dacpac-project' do
  cwd 'C:\Program Files (x86)\Microsoft SQL Server\110\DAC\bin'
  command <<-EOH
  SqlPackage /Action:Publish /SourceFile:"C:\\ivy\\laos\\IvyCpg_Carlsberg_Laos_DBScripts_25_Aug_2015_R61\\IvyCPG_Carlsberg_Laos_DBScripts.dacpac" /TargetDatabaseName:carlsberg_laos /TargetServerName:localhost /TargetUser:sa /TargetPassword:Ivy@123 /Properties:DropIndexesNotInSource=False /Properties:VerifyDeployment=False /variables:IvyCPG_DBProject="IvyCPG_DBProject" > "C:\\Windows\\temp\\product_dacpac.txt" 
    EOH
end


#TODO
#update the site name name in the database in sadm_tenant_configuration

# grant select,update,insert privileges to all tables 
#sql_server_database_user 'foo_user' do
  #connection sql_server_connection_info
  #password 'super_secret'
  #database_name 'foo'
  #privileges [:all]
  #action :grant
#end
