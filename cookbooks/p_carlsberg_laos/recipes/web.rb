zip_name = 'IvyCpg_Product_WebApp_R1248_14 Aug 2015_R1416'
zip_extract_path = 'C:\\temp\\carlsberg_laos'
app_dirname = 'C:\\ivy\\apps\\carlsberg_laos\\IvyCpg_Product_WebApp'
zip_local_path = "C:\\windows\\temp\\ivycpg.zip"

include_recipe 'chocolatey'
chocolatey 'aspnetmvc.install'
chocolatey 'aspnetmvc4.install'
chocolatey 'msaccess2010-redist-x64'
chocolatey 'urlrewrite'
#TODO  2007 Office System Driver: Data Connectivity Components
#TODO https://www.microsoft.com/en-in/download/details.aspx?id=23734

#for error http://www.codeproject.com/Tips/571514/The-page-you-are-requesting-cannot-be-served
#https://technet.microsoft.com/en-us/library/cc754974(v=ws.10).aspx
execute '' do
  cwd 'C:\\Program Files (x86)\\Microsoft SQL Server\\110\\DAC\\bin'
  command <<-EOH
c:\\windows\\system32\\inetsrv\\appcmd set config /section:isapiCgiRestriction /[path='c:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\aspnet_isapi.dll'].allowed:True
    EOH
end

#get the builds from S3
s3_file zip_local_path do
	remote_path "/IvyCpg/IvyCpg_Product/IvyCpg_Product_Web/IvyCpg_Product_WebApp_R1248/IvyCpg_Product_WebApp_R1248_14%20Aug%202015_R1416.zip"
	bucket "ivy-devops-build1"
	aws_access_key_id "AKIAJU54DJFSCV6JJUMQ"
	aws_secret_access_key "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"
	#s3_url "https://s3.amazonaws.com/bucket"
	#owner "me"
	#group "mygroup"
	#mode "0644"
	action :create
	#decryption_key "my SHA256 digest key"
	#decrypted_file_checksum "SHA256 hex digest of decrypted file"
end

if File.directory?(app_dirname)
  FileUtils.remove_dir(app_dirname)
end
FileUtils.mkdir_p(app_dirname)

if File.directory?(zip_extract_path) 
  FileUtils.remove_dir(zip_extract_path)
end

windows_zipfile zip_extract_path do
  source zip_local_path 
  action :unzip
  #not_if {::File.exists?('c:/bin/PsExec.exe')}
end

#directory  do                   
  #rights :full_control, 'Users'
  #inherits false
  #action :create
#end

ruby_block 'Moving Project' do
  block do
    FileUtils.cp_r Dir["C:/Windows/Temp/carlsberg_laos/*/**"],"C:\\ivy\\apps\\carlsberg_laos\\IvyCpg_Product_WebApp" do                 
      rights :full_control, 'Users'
      inherits false
    end
  end
  action :run
end 

#TODO remove passwords from web.confg 
config_path = app_dirname + "\\Web.config"
cookbook_file config_path do
  source 'web.config'
  action :create
end

iis_pool 'carlsberg_laos_pool' do
  runtime_version "4.0"
  pipeline_mode :Classic
  action :add
end

iis_site 'laos' do
  protocol :http
  port 8081 
  #path "#{node['iis']['docroot']}/laos"
  path "C:\\ivy\\laos\\IvyCpg_Product_WebApp"
  host_header "IvyCpg_Product.com"
  action [:add,:start]
  application_pool 'carlsberg_laos_pool'
end


#TODO import reports
#TODO automate mvc4 installation. currently ran exe

