#
# Cookbook Name:: ivy_sqlserver
# Recipe:: default
#
# Copyright 2015, Ivy mobility 
#
# All rights reserved - Do Not Redistribute
#include_recipe 'sql_server::server'
include_recipe 'ms_dotnet45::default'

windows_package 'DACFramework' do
  source 'http://download.microsoft.com/download/9/2/C/92CBE583-5250-4AFD-A87A-DA707065CB9E/ENU/x64/DACFramework.msi'
  action :install
end

windows_package 'SqlDom' do
  source 'http://download.microsoft.com/download/9/2/C/92CBE583-5250-4AFD-A87A-DA707065CB9E/ENU/x64/SqlDom.msi'
  action :install
end

windows_package 'SQLTypes' do
  source 'http://download.microsoft.com/download/9/2/C/92CBE583-5250-4AFD-A87A-DA707065CB9E/ENU/x64/SQLSysClrTypes.msi'
  action :install
end

#{Chef::Config[:file_cache_path]}/

#TODO remove sqlls.msi from files
cookbook_file 'C:\Windows\Temp\sqlls.msi' do
  source 'sqlls.msi'
  action :create
end

windows_package 'sqlls' do
  source 'C:\Windows\Temp\sqlls.msi'
  action :install
end
